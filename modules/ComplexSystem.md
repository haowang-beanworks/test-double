### Example of a "Complex" System
#### Adder
```
public class Adder {
    public int Add(int x, int y){
        if(x<0 && x > 255) throw new Exception;
        if(y<0 && y > 255) throw new Exception;
        result = x + y;
        if(result <0 && result > 255) throw new Exception;
        return result;
    }
}
//Test Cases
// Add(-1, 2) => Statement Coverage
// Add(2, -1) => Statement Coverage
// Add(2, 3) => Statement Coverage
// Add(250, 10) => Statement Coverage
//More
// Add( 257, 1) => Path Coverage
// Add( 1, 257) => Path Coverage
// Result < 0  => Cannot reach

```

+++
#### Mutiplier
```
public class Multiplier {
    Adder addder = new Adder();
    public int Multiple(int x, int y){
        //data check ...
        result=0;
        for(var i=1; i<=y; i++){
            result= adder.Add(result, x);
        }
        //data check ...
        return result;
    }
}
```
@[2]
@[2,7]
+++
#### Mutiplier Test Cases (Continue)
```
//Test Cases
// Data check test cases.. are similar
// Multiple(5, 0) => one loop
// Multiple(5, 1) => two loops
// Multiple(5, 2) => three loops

// Test Cases: the combination of Adders cases and Multiplier itself cases.
// Multiple(-1, 0) => one loop when invalid input 
// Multiple(-1, 1) => two loops when invalid input 
// Multiple(-1, 2) => three loops when invalid input 

// Multiple(257, 0) => one loop when invalid input 
// Multiple(257, 1) => two loops when invalid input 
// Multiple(257, 2) => three loops when invalid input 
even more ......
```
@[1-5]
@[7-10]
@[12-14]
Note: 
omit the data check to focus on other factor


+++
#### Exponent
```
public class ExponentCalculator {
    Multiplier multiplier = new Multiplier();
    public int Calculate(int x, int y){
        //Check data
        result=0;
        for(var i=1; i<=y; i++){
            result= multiplier.multiple(result, x);
        }
        //Check data
        return result;
    }
}
//How many possibility of the size of the test cases here?
// (4 * 9 ) * (13+) > 169   (May just theorily esitmation, but you got it.)

```
@[2]
@[2,7]
@[2,7,13-14]