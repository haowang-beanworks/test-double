## Mocking is a solution
Mocking is a process used in unit testing when the unit being tested has external dependencies. The purpose of mocking is to isolate and focus on the code being tested and not on the behavior or state of external dependencies. In mocking, the dependencies are replaced by closely controlled replacements objects that simulate the behavior of the real ones.

+++
### Terminology: Test Double
Test Double is a generic term for any case where you replace a production object for testing purposes. 
@ul
- **Dummy** objects are passed around but never actually used. Usually they are just used to fill parameter lists.
- **Fake** objects actually have working implementations, but usually take some shortcut which makes them not suitable for production (an InMemoryTestDatabase is a good example).
- **Stubs** provide canned answers to calls made during the test, usually not responding at all to anything outside what's programmed in for the test.
- **Mocks** are *pre-programmed* with expectations which form a specification of the calls they are expected to receive. They can throw an exception if they receive a call they don't expect and are checked during verification to ensure they got all the calls they were expecting.
@ulend

+++
### Mocking need Dependency Injection 
```
public class ExponentCalculator {
    Multiplier multiplier ;
    public ExponentCalculator(Multiplier multiplier){
        this.multiplier = multiplier;
    }
    public int Calculate(int x, int y){        
        result=0;
        for(var i=1; i<=y; i++){
            result= multiplier.Multiple(result, x);
        }       
        return result;
    }
}
```
@[3-5]
+++
### Mocking (Without Magic)
```
public class MockMultipier{
    Func<int,int, int>  mockFunc;
    public MockMultipier(Func<int,int, int> mock){
        mockFunc = mock
    }
    public int Multiple(int x, int y){
        return func(x,y);
    }
}

//Arrange
var multiplier = new MocMultiplier((x, y) => 3);
var calculator = new ExponentCalculator(multiplier);

//Act
var result = calculator.Calculate(3, 10)

//Assert
result.Should().Be(3); 
```
@[1]
@[1,3,4]
@[1,3,4,7]
@[1-9]
@[1-12]
@[1-13]
@[1-16]
@[1-19]

+++ 
### Mocking by NSubstitute
```
//Arrange
var multiplier = Substitute.For<Multiplier>();
multiplier.Multiple(3, 3).Returns(3);
var calculator = new ExponentCalculator(multiplier);

//Act
var result = calculator.Calculate(3, 10)

//Assert
result.Should().Be(3); // state check => Stub
multiplier.Recieved(9).Add(3,3); //behaviour chec: loop and call multiplier 9 times => Mock
```
@[1-2](Create Mock Object)
@[1-3](Mocking function)
@[1-4]
@[1-7]
@[1-11]

+++
### Benfits
@ul
- Unit is Unit: relief you from the dependencies implementation
- Size of Tests: exponential growth to linear growth
- Framework: reduce mocking plumbing codes and focus on business
- Development: from the Bottom Up to the Top Down
@ulend

+++
### Bottom Up vs Top Down
![Top-Down-Bottom-up](assets/img/Top-Down-Bottom-up.png)

+++
### More about NSubstitute
@ul
- Setting return value: `object.Property.Returns(...);`
- Specify argument: `obj.Method(Arg.Any<int>).Returns(...);`
- Multiple return values: `obj.Method().Returns(3, 5, 6)`
- Checking received calls: `obj.DidNotReceive().Method(Arg.Is<int>(x => x >= 500))`
- Checking call order : `Received.InOrder(() => { obj.Method1(); obj.Method2()});`
@ulend

+++
### More Frameworks
@ul
- Rhino Mocks (c#)
- Moq (C#)
- PHPUnit/Mockery (PHP)
- Mocha (Javascript)
@ulend

