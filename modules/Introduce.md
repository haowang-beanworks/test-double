### TDD: from simple case to real system
@ul 
- Money (TDD 17):        160 LoC
- SyncTool:             12,115 LoC
- BW API:               ?????? LoC
@ulend
 
Note:
Multi-Currency Money
We are not just facing your code but the all system
17 Chapters in the DDD book

+++
### Money Classes Diagram
![Multi-Currency-Money-Dependency](assets/img/Multi-Currency-Money-DependencyGraphSnapshot.png)

Note: 
- 17 Chapters in the DDD book

+++
### SyncTool Modules Diagram
![SyncTool-Depenency](assets/img/SyncTool-DependencyGraphSnapshot.png)

+++ 
#### Size Comparision
![Earth-in-Galaxy](assets/img/Earth-in-Galaxy.jpg)
You think it's the whole world, but actually it is just a grain of sand.

Note:

+++ 
#### Still Acceptable if Linear Growth of Test Code
![](assets/img/Linear-Growth-Test-Code.png)

+++ 
#### Really Problem when Exponential Growth of Test Code
![](assets/img/Exponential-Growth-Test-Code.png)

