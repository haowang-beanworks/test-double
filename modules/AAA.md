
### The Anatomy of a Unit Test
@ul
- Arrange
- Act
- Assert
@ulend
Called AAA pattern

Note:
The AAA pattern advocates for splitting each test into three parts: arrange, act, and assert
+++
### Big Mud of Ball
#### Productivity vs. Time
![Productivity vs. time](assets/img/Productivity-Time.jpg)

+++
### Is this your engineer life cycle?
![Clean Slate](assets/img/Clean-Slate.jpeg)